<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191203083117 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE discussion_comment (id INT AUTO_INCREMENT NOT NULL, discussion_id INT DEFAULT NULL, content LONGTEXT NOT NULL, static_name VARCHAR(255) NOT NULL, static_email VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_1C2A74D11ADED311 (discussion_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE discussion (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, static_name VARCHAR(255) NOT NULL, static_email VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, code VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE discussion_comment ADD CONSTRAINT FK_1C2A74D11ADED311 FOREIGN KEY (discussion_id) REFERENCES discussion (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE discussion_comment DROP FOREIGN KEY FK_1C2A74D11ADED311');
        $this->addSql('DROP TABLE discussion_comment');
        $this->addSql('DROP TABLE discussion');
    }
}
