<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Skadmin\Discussion\BaseControl;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191203082359 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $data = [
            'base_control' => BaseControl::class,
            'webalize'     => 'discussion',
            'name'         => 'Discussion',
            'is_active'    => 1,
        ];

        $this->addSql(
            'INSERT INTO core_package (base_control, webalize, name, is_active) VALUES (:base_control, :webalize, :name, :is_active)',
            $data
        );

        $resources = [
            [
                'name'        => 'discussion',
                'title'       => 'role-resource.discussion.title',
                'description' => 'role-resource.discussion.description',
            ],
        ];

        foreach ($resources as $resource) {
            $this->addSql('INSERT INTO core_role_resource (name, title, description) VALUES (:name, :title, :description)', $resource);
        }
    }

    public function down(Schema $schema) : void
    {
    }
}
