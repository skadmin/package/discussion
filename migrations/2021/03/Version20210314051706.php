<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210314051706 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $translations = [
            ['original' => 'discussion.overview', 'hash' => 'b8835bee479a90e93350d0ff3b62ca1f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Diskuze', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.discussion.title', 'hash' => '14e15c52e36d0c44d6291b71960cd843', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Diskuze', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.discussion.description', 'hash' => '3b4f1f6741a4d00935c387f16cf59e92', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat diskuze', 'plural1' => '', 'plural2' => ''],
            ['original' => 'discussion.overview.title', 'hash' => '230b134da7ed874e815bb24345f9313a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Diskuze|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.discussion.overview.status.unarchive', 'hash' => '61a4e4105811ecb9b30a081d8018c22b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'ne', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.discussion.overview.status.archive', 'hash' => '4e4b52a22b41034ceea8c6ab07d29d1d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'ano', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.discussion.overview.is-active', 'hash' => '15f6719b62289ce5b3eba155730b54f6', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je archivovaná?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.discussion.overview.name', 'hash' => '8b5f48be8844d11de43adcce534a5536', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.discussion.overview.static-name', 'hash' => '05b6023866dc65ef0b7e0ea230f7aa98', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Autor', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.discussion.overview.created-at', 'hash' => 'a8a7a5f0c241514d354fc4c8b978fed9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vytvořeno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.discussion.overview.action.archive', 'hash' => 'd54d473ce490251da5d137ec3186dd00', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.discussion.overview.action.archive.title', 'hash' => 'bc60b81d74e18457432f150e7b823a7b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'přesunout do archívu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.discussion.overview.action.unarchive', 'hash' => '022b431fecaa217c4cf079090a6fc1f9', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.discussion.overview.action.unarchive.title', 'hash' => 'cc8b8a6cf32beb1353148bf7ca22e2ea', 'module' => 'admin', 'language_id' => 1, 'singular' => 'přesunout z archívu', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
