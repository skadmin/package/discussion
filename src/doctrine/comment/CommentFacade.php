<?php

declare(strict_types=1);

namespace Skadmin\Discussion\Doctrine\Comment;

use SkadminUtils\DoctrineTraits\Facade;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Discussion\Doctrine\Discussion\Discussion;

final class CommentFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Comment::class;
    }

    public function create(Discussion $discussion, string $content, string $userName, string $userEmail) : Comment
    {
        $comment = $this->get();

        $comment->create($discussion, $content, $userName, $userEmail);

        $this->em->persist($comment);
        $this->em->flush();

        return $comment;
    }

    public function get(?int $id = null) : Comment
    {
        if ($id === null) {
            return new Comment();
        }

        $comment = parent::get($id);

        if ($comment === null) {
            return new Comment();
        }

        return $comment;
    }
}
