<?php

declare(strict_types=1);

namespace Skadmin\Discussion\Doctrine\Comment;

use SkadminUtils\DoctrineTraits\Entity;
use Skadmin\Discussion\Doctrine\Discussion\Discussion;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'discussion_comment')]
#[ORM\HasLifecycleCallbacks]
class Comment
{
    use Entity\Id;
    use Entity\Content;
    use Entity\StaticUserData;
    use Entity\Created;

    #[ORM\ManyToOne(targetEntity: Discussion::class, inversedBy: 'comments')]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Discussion $discussion;

    public function create(Discussion $discussion, string $content, string $userName, string $userEmail) : void
    {
        $this->discussion  = $discussion;
        $this->content     = $content;
        $this->staticName  = $userName;
        $this->staticEmail = $userEmail;
    }

    public function getDiscussion() : Discussion
    {
        return $this->discussion;
    }
}
