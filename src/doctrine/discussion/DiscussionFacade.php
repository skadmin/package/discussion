<?php

declare(strict_types=1);

namespace Skadmin\Discussion\Doctrine\Discussion;

use SkadminUtils\DoctrineTraits\Facade;
use Nette\Utils\Random;
use Nette\Utils\Strings;
use Nettrine\ORM\EntityManagerDecorator;

final class DiscussionFacade extends Facade
{
    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = Discussion::class;
    }

    public function create(string $name, string $userName, string $userEmail) : Discussion
    {
        $code          = Strings::upper(Random::generate());
        $existWithCode = $this->existWithCode($code);

        while ($existWithCode) {
            $code          = Strings::upper(Random::generate());
            $existWithCode = $this->existWithCode($code);
        }

        $discussion = $this->get();

        $discussion->create($name, $userName, $userEmail, $code);

        $this->em->persist($discussion);
        $this->em->flush();

        return $discussion;
    }

    public function existWithCode(string $code) : bool
    {
        $criteria = ['code' => $code];

        $discussion = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $discussion !== null;
    }

    public function get(?int $id = null) : Discussion
    {
        if ($id === null) {
            return new Discussion();
        }

        $discussion = parent::get($id);

        if ($discussion === null) {
            return new Discussion();
        }

        return $discussion;
    }


    public function archive(Discussion $discussion) : Discussion
    {
        $discussion->archive();

        $this->em->persist($discussion);
        $this->em->flush();

        return $discussion;
    }

    public function unarchive(Discussion $discussion) : Discussion
    {
        $discussion->unarchive();

        $this->em->persist($discussion);
        $this->em->flush();

        return $discussion;
    }

    /**
     * @return Discussion[]
     */
    public function getUnarchiveDiscussions() : array
    {
        $criteria = ['isActive' => true];
        $order    = ['createdAt' => 'DESC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $order);
    }
}
