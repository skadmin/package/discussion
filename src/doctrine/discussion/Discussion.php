<?php

declare(strict_types=1);

namespace Skadmin\Discussion\Doctrine\Discussion;

use SkadminUtils\DoctrineTraits\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Skadmin\Discussion\Doctrine\Comment\Comment;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Discussion
{
    use Entity\Id;
    use Entity\Name;
    use Entity\StaticUserData;
    use Entity\Created;
    use Entity\Code;
    use Entity\IsActive;

    /** @var ArrayCollection<int, Comment> */
    #[ORM\OneToMany(targetEntity: Comment::class, mappedBy: 'discussion')]
    #[ORM\OrderBy(['createdAt' => 'DESC'])]
    private $comments;

    public function __construct()
    {
        $this->isActive = true;
    }

    public function create(string $name, string $userName, string $userEmail, ?string $code = null) : void
    {
        $this->name        = $name;
        $this->staticName  = $userName;
        $this->staticEmail = $userEmail;

        if ($code === null) {
            return;
        }

        $this->code = $code;
    }

    /**
     * @return Comment[]|ArrayCollection
     */
    public function getComments()
    {
        return $this->comments;
    }

    public function archive() : void
    {
        $this->isActive = false;
    }

    public function unarchive() : void
    {
        $this->isActive = true;
    }
}
