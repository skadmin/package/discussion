<?php

declare(strict_types=1);

namespace Skadmin\Discussion\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use Skadmin\Discussion\BaseControl;
use Skadmin\Discussion\Doctrine\Discussion\DiscussionFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;

/**
 * Class Detail
 */
class Overview extends TemplateControl
{
    use APackageControl;

    /** @var DiscussionFacade */
    private $facade;

    /** @var Create */
    private $formCreate;

    public function __construct(DiscussionFacade $facade, Translator $translator, ICreateFactory $iCreateFactory)
    {
        parent::__construct($translator);
        $this->facade = $facade;

        $this->formCreate = $iCreateFactory->create();
        $this->formCreate->setDrawBox(false);
    }

    public function getTitle() : string
    {
        return 'discussion.front.overview.title';
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');

        $template->package = new BaseControl();

        $template->discussions = $this->facade->getUnarchiveDiscussions();

        $template->render();
    }

    public function formCreateOnRedraw() : void
    {
        $this->redrawControl('snipOverview');
    }

    /**
     * @param SimpleTranslation|string $message
     */
    public function formCreateOnFlashmessage($message, string $type) : void
    {
        $this->onFlashmessage($message, $type);
    }

    protected function createComponentFormCreate(string $name) : Create
    {
        $control                   = $this->formCreate;
        $control->onFlashmessage[] = [$this, 'formCreateOnFlashmessage'];
        $control->onRedraw[]       = [$this, 'formCreateOnRedraw'];
        return $control;
    }
}
