<?php

declare(strict_types=1);

namespace Skadmin\Discussion\Components\Front;

/**
 * Interface IOverviewFactory
 */
interface IDetailFactory
{
    public function create(int $id) : Detail;
}
