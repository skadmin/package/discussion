<?php

declare(strict_types=1);

namespace Skadmin\Discussion\Components\Front;

use App\Components\Grid\TemplateControl;
use App\Model\System\APackageControl;
use Skadmin\Discussion\BaseControl;
use Skadmin\Discussion\Doctrine\Discussion\Discussion;
use Skadmin\Discussion\Doctrine\Discussion\DiscussionFacade;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;

/**
 * Class Detail
 */
class Detail extends TemplateControl
{
    use APackageControl;

    /** @var DiscussionFacade */
    private $facade;

    /** @var Comment */
    private $formComment;

    /** @var Discussion */
    private $discussion;

    public function __construct(int $id, DiscussionFacade $facade, Translator $translator, ICommentFactory $iCommentFactory)
    {
        parent::__construct($translator);
        $this->facade = $facade;

        $this->discussion = $this->facade->get($id);

        $this->formComment = $iCommentFactory->create($this->discussion);
        $this->formComment->setDrawBox(false);
    }

    public function getTitle() : SimpleTranslation
    {
        return new SimpleTranslation('discussion.front.detail.title - %s', $this->discussion->getName());
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/detail.latte');

        $template->package = new BaseControl();

        $template->discussion = $this->discussion;

        $template->render();
    }

    public function formCommentOnRedraw() : void
    {
        $this->redrawControl('snipComments');
    }

    /**
     * @param SimpleTranslation|string $message
     */
    public function formCommentOnFlashmessage($message, string $type) : void
    {
        $this->onFlashmessage($message, $type);
    }

    protected function createComponentFormComment(string $name) : Comment
    {
        $control                   = $this->formComment;
        $control->onFlashmessage[] = [$this, 'formCommentOnFlashmessage'];
        $control->onRedraw[]       = [$this, 'formCommentOnRedraw'];
        return $control;
    }
}
