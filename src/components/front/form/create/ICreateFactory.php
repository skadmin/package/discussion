<?php

declare(strict_types=1);

namespace Skadmin\Discussion\Components\Front;

/**
 * Interface IEditFactory
 */
interface ICreateFactory
{
    public function create() : Create;
}
