<?php

declare(strict_types=1);

namespace Skadmin\Discussion\Components\Front;

use App\Components\Form\FormWithUserControl;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Discussion\Doctrine\Discussion\DiscussionFacade;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;

/**
 * Class Edit
 */
class Create extends FormWithUserControl
{
    use APackageControl;

    /** @var DiscussionFacade */
    private $facade;

    public function __construct(DiscussionFacade $facade, Translator $translator, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade = $facade;
    }

    public function getTitle() : string
    {
        return 'form.discussion.front.create.title';
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        $discussion = $this->facade->create($values->name, $this->loggedUser->getIdentity()->getFullName(), $this->loggedUser->getIdentity()->getEmail());

        $form->reset();
        $this->redrawControl('snipForm');

        $this->onRedraw();
        $this->onFlashmessage('form.discussion.front.create.flash.success', Flash::SUCCESS);
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/create.latte');

        $template->drawBox = $this->drawBox;

        $template->render();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        $form->addText('name', 'form.discussion.front.create.name')
            ->setRequired('form.discussion.front.create.name.req')
            ->setHtmlAttribute('autocomplete', 'off');

        // BUTTON
        $form->addSubmit('send', 'form.discussion.front.create.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }
}
