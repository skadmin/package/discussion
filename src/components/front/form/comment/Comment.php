<?php

declare(strict_types=1);

namespace Skadmin\Discussion\Components\Front;

use App\Components\Form\FormWithUserControl;
use App\Model\System\APackageControl;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Skadmin\Discussion\Doctrine\Comment\CommentFacade;
use Skadmin\Discussion\Doctrine\Discussion\Discussion;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;

/**
 * Class Comment
 */
class Comment extends FormWithUserControl
{
    use APackageControl;

    /** @var CommentFacade */
    private $facade;

    /** @var Discussion */
    private $discussion;

    public function __construct(Discussion $discussion, CommentFacade $facade, Translator $translator, LoggedUser $user)
    {
        parent::__construct($translator, $user);
        $this->facade = $facade;

        $this->discussion = $discussion;
    }

    public function getTitle() : string
    {
        return 'form.comment.front.create.title';
    }

    public function processOnSuccess(Form $form, ArrayHash $values) : void
    {
        $comment = $this->facade->create($this->discussion, $values->content, $this->loggedUser->getIdentity()->getFullName(), $this->loggedUser->getIdentity()->getEmail());

        $form->reset();
        $this->redrawControl('snipForm');

        $this->onRedraw();
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/comment.latte');

        $template->drawBox     = $this->drawBox;
        $template->socketId    = $this->discussion->getId();
        $template->socketToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJlZTM0ODZmMDA1NDY5OTRkZTRlMzg4M2NkOTdhMzIyNWY4MGFhZWYwMDVmYTVjYmYzYzI2N2UyMzJiYzBkZjZiYzFkMGFmZjk5ZjZkNjU0In0.eyJhdWQiOiI2IiwianRpIjoiYmVlMzQ4NmYwMDU0Njk5NGRlNGUzODgzY2Q5N2EzMjI1ZjgwYWFlZjAwNWZhNWNiZjNjMjY3ZTIzMmJjMGRmNmJjMWQwYWZmOTlmNmQ2NTQiLCJpYXQiOjE1NzU0NDczMTcsIm5iZiI6MTU3NTQ0NzMxNywiZXhwIjoxNjA3MDY5NzE3LCJzdWIiOiIxMTkiLCJzY29wZXMiOltdfQ.P7q220VBS7uM8_IocgcRbI-h_VBBE17wc7IZhmYsbIzLF_ZFKt42phAUapu6RA7ehOS7_TM_3w-q6AtAjppsCyf54aUcfR8LTt1iYFc3WCCySIBhpESX74wM-ChPVAGTl5hZcCE8m7K9FuGXFtZrm0h74c-UWZNkjcKGu5lt3oUCPMGM8D62jWx193qT8NRiTMTxjgqjvQW-BalOl6_29n7st3Q_XKLXpgJsE4U1sz_2CvBljqoA0PnvmythRndKkkxHKT1Vl6iPWv4nhhGYFHlXXEQad_njjNT5o7MUJvw6nP7pWWBiUbPbWdVKged54Dn565P8U_gIffmKBur-dZJFnuoLY6WydGpEMgb937NmjvuB4dQ-jJp2seFvQUt7MwGMpyRWrUkYdsVKLmvH9ttrYejFmMdC5EHwMQtHFoUshZqp5ljKAeviClRbga4KwA37ZrQoGT1S6dsNafQTntpMmoKB_CyIJzOmm2i41dIFm6mY2gslDkXcugSIA_U8nrg_hY4hoAsRGuIN2urMvHVOXBzm_nS_IhJ-Bt41s8hFicqvjVtQdg3AveG6Rl0YAB5--KEHc-Tn-vf226h8pn3vvW3Ip6o1waWpaVTz87w9kFY8BjeChjR22jgeRUQiiGwxzQQpDeP8pR730AiOab7n-avnB93ggqrEn4LQrR0';

        $template->render();
    }

    public function handleInvalidateRedraw() : void
    {
        $this->onRedraw();
    }

    protected function createComponentForm() : Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        $form->addTextArea('content', 'form.comment.front.create.content', null, 5)
            ->setRequired('form.comment.front.create.content.req');

        // BUTTON
        $form->addSubmit('send', 'form.comment.front.create.send');

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }
}
