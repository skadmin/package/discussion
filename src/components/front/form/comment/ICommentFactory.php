<?php

declare(strict_types=1);

namespace Skadmin\Discussion\Components\Front;

use Skadmin\Discussion\Doctrine\Discussion\Discussion;

/**
 * Interface ICommentFactory
 */
interface ICommentFactory
{
    public function create(Discussion $discussion) : Comment;
}
