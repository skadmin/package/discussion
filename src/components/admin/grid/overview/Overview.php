<?php

declare(strict_types=1);

namespace Skadmin\Discussion\Components\Admin;

use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use Skadmin\Role\Doctrine\Role\Privilege;
use App\Model\System\APackageControl;
use App\Model\System\Constant;
use App\Model\System\Utils;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\Discussion\BaseControl;
use Skadmin\Discussion\Doctrine\Discussion\Discussion;
use Skadmin\Discussion\Doctrine\Discussion\DiscussionFacade;
use Skadmin\Translator\Translator;
use function intval;

/**
 * Class Overview
 */
class Overview extends GridControl
{
    use APackageControl;

    public const ARCHIVE   = 0;
    public const UNARCHIVE = 1;

    public const STATUS_ARCHIVE = [
        self::UNARCHIVE => 'grid.discussion.overview.status.unarchive',
        self::ARCHIVE   => 'grid.discussion.overview.status.archive',
    ];

    /** @var DiscussionFacade */
    private $facade;

    public function __construct(DiscussionFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null)
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render() : void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle() : string
    {
        return 'discussion.overview.title';
    }

    protected function createComponentGrid(string $name) : GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel());

        // DATA
        $dataArchive = Arrays::map(self::STATUS_ARCHIVE, function ($item) : string {
            return $this->translator->translate($item);
        });

        // COLUMNS
        $grid->addColumnText('name', 'grid.discussion.overview.name')
            ->setRenderer(static function (Discussion $discussion) : Html {
                $name = Html::el('span', ['class' => 'text-primary font-weight-bold'])->setText($discussion->getName());

                $code = Html::el('code', ['class' => 'text-muted small'])->setText($discussion->getCode());

                $sender = new Html();
                $sender->addHtml($name)
                    ->addHtml('<br/>')
                    ->addHtml($code);

                return $sender;
            });
        $grid->addColumnText('sender', 'grid.discussion.overview.static-name')
            ->setRenderer(static function (Discussion $discussion) : Html {
                $sender = new Html();
                $sender->addText($discussion->getStaticName())
                    ->addHtml('<br/>')
                    ->addHtml(Utils::createHtmlContact($discussion->getStaticEmail()));

                return $sender;
            });
        $grid->addColumnDateTime('createdAt', 'grid.discussion.overview.created-at')
            ->setFormat('d.m.Y H:i')
            ->setSortable();
        $grid->addColumnText('isActive', 'grid.discussion.overview.is-active')
            ->setAlign('center')
            ->setRenderer(static function (Discussion $discussion) use ($dataArchive) : Html {
                $status = Html::el('span');

                if ($discussion->isActive()) {
                    $status->setText($dataArchive[self::UNARCHIVE]);
                } else {
                    $status->setText($dataArchive[self::ARCHIVE])
                        ->addAttributes(['class' => 'text-danger']);
                }

                return $status;
            });

        // FILTER
        $grid->addFilterText('name', 'grid.discussion.overview.name', ['name', 'code']);
        $grid->addFilterText('sender', 'grid.discussion.overview.static-name', ['staticName', 'staticEmail']);
        $grid->addFilterSelect('isActive', 'grid.discussion.overview.is-active', $dataArchive)
            ->setPrompt(Constant::PROMTP);

        // ACTION
        $grid->addActionCallback('archive', 'grid.discussion.overview.action.archive')
            ->setIcon('folder')
            ->setTitle('grid.discussion.overview.action.archive.title')
            ->setClass('btn btn-xs btn-outline-danger ajax')
            ->onClick[] = function (string $discussionId) use ($grid) : void {
                $discussion = $this->facade->get(intval($discussionId));
                $this->facade->archive($discussion);

                $grid->redrawItem($discussionId);
            };

        $grid->addActionCallback('unarchive', 'grid.discussion.overview.action.unarchive')
            ->setIcon('folder-open')
            ->setTitle('grid.discussion.overview.action.unarchive.title')
            ->setClass('btn btn-xs btn-outline-primary ajax')
            ->onClick[] = function (string $discussionId) use ($grid) : void {
                $discussion = $this->facade->get(intval($discussionId));
                $this->facade->unarchive($discussion);

                $grid->redrawItem($discussionId);
            };

        // ALLOWED ACTION
        $grid->allowRowsAction('archive', static function (Discussion $discussion) : bool {
            return $discussion->isActive();
        });

        $grid->allowRowsAction('unarchive', static function (Discussion $discussion) : bool {
            return ! $discussion->isActive();
        });

        // DETAIL
        $grid->setItemsDetail(__DIR__ . '/detail.latte');

        // OTHER
        $grid->setDefaultSort(['createdAt' => 'DESC']);
        $grid->setDefaultFilter([
            'isActive' => self::UNARCHIVE,
        ]);

        return $grid;
    }
}
